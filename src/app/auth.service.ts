import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from './interfaces/user';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
 

  
  constructor(public afAuth:AngularFireAuth,
              public router:Router) { 
      this.user = this.afAuth.authState;
  }

  user:Observable <any | null>
  public err:any;
  

  logout(){
    this.afAuth.auth.signOut()
    this.router.navigate(['/greetings'])
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/greetings']);
            })
        .catch (
          error => {this.err = error
          }) 
   }

  }