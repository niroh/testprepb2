import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent implements OnInit {

  constructor(public authservice:AuthService) { }

  userData$: Observable<any>;
  email:string

  ngOnInit() {
    this.userData$=this.authservice.user;
    
    this.userData$.subscribe(
      data => {
        this.email = data.email;
      } 
    )
  }

}
