export interface User {
    uid:string,
    email?: string | null,
    password?:string,
    photoUrl?: string,
    displayName?:string
    checkbox?:boolean;
}
