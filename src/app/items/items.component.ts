import { AngularFirestore } from '@angular/fire/firestore';
import { ItemsService } from './../items.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  constructor(public itemsservice:ItemsService,
              public db:AngularFirestore) { 
  }

  items$:Observable<any>;
  stock:boolean = true;
  beforeDelete: boolean = false;

  ngOnInit() {
    this.items$ = this.itemsservice.getItems();
  }

  click(){
    this.beforeDelete = true;
  }
  
  changeStock(id:string, stock:boolean){
    this.itemsservice.changeState(id, stock)
  }



  deleteItem(id:string){
    this.itemsservice.deleteItem(id);
  }

}
