// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCtS6Zb9NApAulH_EZcUBF5i-eQMDXz2dk",
    authDomain: "testprepb2.firebaseapp.com",
    databaseURL: "https://testprepb2.firebaseio.com",
    projectId: "testprepb2",
    storageBucket: "testprepb2.appspot.com",
    messagingSenderId: "681534445718",
    appId: "1:681534445718:web:108cd6ac0a24db77888ab7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
